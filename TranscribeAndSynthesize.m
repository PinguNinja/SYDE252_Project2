function [ song, fs ] = TranscribeAndSynthesize(file_name, tempo_num)
    [y, fs] = audioread(file_name);
    [a, b, c] = tempo2(y(:, 1), fs);
    if a > 0.5
       bpm = a(1); 
    else
       bpm = a(2);
    end
    bps = bpm / 60;
    nps = 1/bps/tempo_num;

    T = 1/fs;
    window_size = nps * fs;
    keys = 1:88;
    frequencies = 2.^((keys - 49)/12) * 440;
    minute_length = fs * 60;
    original_song = y(1:minute_length, 1);
    [s, f, t, ps] = spectrogram(original_song, window_size, 0, frequencies, fs);

    amplitude = abs(s);
    phase = angle(s);
    amplitude = amplitude / max(max(amplitude));

    length = 60 * fs;
    song = zeros(1, length);
    counter = 0;
    prev_time = 0;
    for i = 1:size(amplitude, 2)
        end_time = 2*(t(i) - prev_time);
        time = 0:T:end_time;
        prev_time = prev_time + end_time;
        for j = 1:size(amplitude, 1)
            section = (amplitude(j, i) * sin(2*pi*frequencies(j)*time + phase(j, i)) );
            song(counter + 1:counter+size(section, 2)) = song(counter + 1:counter+size(section, 2)) + section;
        end
        counter = counter+size(section, 2);
    end
    scaling_factor = sum(original_song .^ 2) / sum(song .^ 2);
    song = song * sqrt(scaling_factor);

    plot(original_song);
    hold on;
    plot(song);

end

