clear all; clc;

[bach, Fs_bach] = audioread('E:\CodeBin\Syde252\Project2\bach_850.mp3');
[chpn, Fs_chpn] = audioread('E:\CodeBin\Syde252\Project2\chpn_op7_1.mp3');
[elise, Fs_elise] = audioread('E:\CodeBin\Syde252\Project2\elise.mp3');

tempo_bach = tempo2(bach(:,1), Fs_bach);
tempo_chpn = tempo2(chpn(:,1), Fs_chpn);
tempo_elise = tempo2(elise(:,1), Fs_elise);

beat_bach = beat2(bach(:,1), Fs_bach);
beat_chpn = beat2(chpn(:,1), Fs_chpn);
beat_elise = beat2(elise(:,1), Fs_elise);

bach_synth = mkblips(beat_bach, Fs_bach,length(bach));
% Listen to them mixed together
soundsc(bach+bach_synth, Fs_bach);

