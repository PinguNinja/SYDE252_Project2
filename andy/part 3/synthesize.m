function [song, fs] = synthesize(notes, fs, note_dur)
% TODO: 
% -find correct note duration
% -BUG: loud click sound at the end of every note

%Key number to freq
for i = 1:length(notes)
    f(i) = 440 * 2.^((notes(i)-49)/12);
end

t=0:1/fs:note_dur; 
song = [];

for i = 1:length(f)
    song = [song sin(2*pi*f(i).*t)];
end