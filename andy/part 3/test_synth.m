clear all; clc;

% {'A' 'A' 'E' 'E' 'F#' 'F#' 'E' 'E' 'D' 'D' 'C#' 'C#' 'B' 'B' 'A' 'A'}
notes = [49,49,56,56,58,58,56,56,54,54,53,53,51,51,49,49];

[song, fs] = synthesize(notes, 44100, 0.3);

sound(song, fs)