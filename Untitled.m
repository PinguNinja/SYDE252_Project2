[Y, FS] = audioread('bach_850.mp3');
sound(Y,FS);
filterband = filterbank(Y(3103293:5103293,1));
hwind = hwindow(filterband);
diffout = diffrect(hwind);

iter1 = timecomb(diffout,2,60,240);
iter2 = timecomb(diffout,2,iter1-5,iter1+5);

